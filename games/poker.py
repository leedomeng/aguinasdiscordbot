import random
import itertools

class Result:
  straight_flush = 8
  fourcard = 7
  fullhouse = 6
  flush = 5
  straight = 4
  triple = 3
  two_pair = 2
  one_pair = 1
  no_pair = 0

  @staticmethod
  def get_str(v):
    renum = '2 3 4 5 6 7 8 9 10 J Q K A'.split()

    dyn = ['노 페어','원 페어','투 페어','트리플','스트레이트','플러쉬','풀하우스','포카드', '스트레이트 플러쉬']
    if v[0] in [Result.fullhouse, Result.two_pair]:
      code = renum[v[1][0]] + "/" + renum[v[1][1]]
    elif v[0] == Result.no_pair:
      return dyn[v[0]] + "("+renum[v[1][0]]+"탑)"
    else:
      code = renum[v[1][0]]
    return code + " " + dyn[v[0]]
  
def judge(cards):
  nums = sorted([x[1] for x in cards])

  is_flush = (len(set([x[0] for x in cards])) == 1)
  is_straight = False
  if len(set(nums)) == 5:
    is_straight = (nums[0] + 4 == nums[-1])
    if nums[0] == 0 and nums[0]+3 == nums[-2] and nums[-1]==12:
      is_straight = True
  if is_straight and is_flush:
    return Result.straight_flush, tuple(reversed(nums))
  pairs = []
  triples = []
  for i in set(nums):
    if nums.count(i) == 2:
      pairs += [i]
    if nums.count(i) == 3:
      triples += [i]
    if nums.count(i) == 4:
      return Result.fourcard, (i,)
  pairs = sorted(pairs)
  nums = list(set(nums))
  if len(pairs) > 0 and len(triples) > 0:
    return Result.fullhouse, (triples[0], pairs[0])
  if is_flush:
    return Result.flush, tuple(reversed(nums))
  if is_straight:
    return Result.straight, tuple(reversed(nums))
  if len(triples) > 0:
    nums.remove(triples[0])
    return Result.triple, (triples[0], nums[-1], nums[-2])
  if len(pairs) > 1:
    nums.remove(pairs[0])
    nums.remove(pairs[1])
    return Result.two_pair, (pairs[-1], pairs[-2], nums[0])
  if len(pairs) > 0:
    nums.remove(pairs[0])
    return Result.one_pair, (pairs[0], nums[-1], nums[-2], nums[-3])
  return Result.no_pair, tuple(reversed(nums))

def random_cards(servername):
  renum = '23456789TJQKA'
  types = ['♠','♡','◇','♣'] 
  if servername == 'aguinas':
    types = ['<:cspade:309575832601362432>','<:cheart:309576467099025418>','<:cdia:309576524707921931>','<:cclover:309576645155749888>']
  deck = list(itertools.product(types,range(len(renum))))
  #1. 7 poker
  cards = sorted(random.sample(deck,7),key=lambda x:(x[1],x[0]))
  vs = max([(judge(vv),vv) for vv in itertools.combinations(cards,5)])
  cards = vs[1]
  judge_result = Result.get_str(vs[0])
  #2. 5 poker
  # cards = sorted(random.sample(deck,5),key=lambda x:(x[1],x[0]))
  if servername == 'aguinas':
    disp = ' '.join([j[0]+' **'+renum[j[1]]+'**' for j in cards]).replace('T','10')
    return disp + ' - ' + judge_result
  elif servername == 'test':
    return judge_result
  else:
    return '```' + ' '.join([j[0] +' ' + renum[j[1]] for j in cards]).replace('T','10') + ' - ' + judge_result + '```'

win_lose_cnt = {}
def match(servername, name_a, name_b):
  global win_lose_cnt
  renum = '23456789TJQKA'
  types = ['♠','♡','◇','♣'] 
  if servername == 'aguinas':
    types = ['<:cspade:309575832601362432>','<:cheart:309576467099025418>','<:cdia:309576524707921931>','<:cclover:309576645155749888>']
  deck = list(itertools.product(types,range(len(renum))))
  dummy = random.sample(deck,14)
  cards_a = sorted(dummy[:7],key=lambda x:(x[1],x[0]))
  cards_b = sorted(dummy[7:],key=lambda x:(x[1],x[0]))
  vs_a = max([(judge(vv),vv) for vv in itertools.combinations(cards_a,5)])
  vs_b = max([(judge(vv),vv) for vv in itertools.combinations(cards_b,5)])
  win_lose = '무승부..'
  winner,loser = None, None
  if vs_a[0] > vs_b[0]: #or (vs_a[0][0] == vs_b[0][0] and vs_a[0][1] > vs_b[0][1]):
    win_lose = '**' + name_a + '승!**'
    winner,loser = name_a,name_b
  elif vs_b[0] > vs_a[0]: # or (vs_a[0][0] == vs_b[0][0] and vs_b[0][1] > vs_a[0][1]):
    win_lose = '**' + name_b + '승!**'
    winner,loser = name_b,name_a
  if servername == 'aguinas':
    disp = "{name} : {show} - {result}\n".format(name=name_a, 
      show=' '.join([j[0]+' **'+renum[j[1]]+'**' for j in vs_a[1]]).replace('T','10'),
      result=Result.get_str(vs_a[0]) )
    disp += "{name} : {show} - {result}\n".format(name=name_b, 
      show=' '.join([j[0]+' **'+renum[j[1]]+'**' for j in vs_b[1]]).replace('T','10'),
      result=Result.get_str(vs_b[0]) )
  elif servername == 'test':
    if vs_a[0] > vs_b[0]:
      return 'A'
    if vs_a[0] < vs_b[0]:
      return 'B'
    return 'D'
  else:
    disp = '```'
    disp += "{name} : {show} - {result}\n".format(name=name_a, 
      show=' '.join([j[0]+' '+renum[j[1]] for j in vs_a[1]]).replace('T','10'),
      result=Result.get_str(vs_a[0]) )
    disp += "{name} : {show} - {result}\n".format(name=name_b, 
      show=' '.join([j[0]+' '+renum[j[1]] for j in vs_b[1]]).replace('T','10'),
      result=Result.get_str(vs_b[0]) )
    disp += '```\n'
  disp += win_lose
  if not winner is None:
    w,l = win_lose_cnt.get(servername +'.' + winner,(0,0))
    w,l = w+1,0
    if w >= 2:
      disp += " `{} {}연승{}`".format(winner,w,min(w,6)*'!')
    win_lose_cnt[servername +'.' +winner] = (w,l)
  if not loser is None:
    w,l = win_lose_cnt.get(servername +'.' +loser,(0,0))
    if w >= 2:
      disp += " `{}의 {}연승 저지!`".format(loser,w+1)
    w,l = 0,l+1
    if l >= 2:
      disp += " `{} {}연패{}`".format(loser,l,min(l,6)*'.')
    win_lose_cnt[servername +'.' +loser] = (w,l)
  return disp

def table():
  return '''```스트레이트 플러쉬   0.070%
포카드   0.210%
풀하우스   2.480%
플러쉬   3.150%
스트레이트   4.560%
트리플   5.020%
투 페어   22.970%
원 페어   43.990%
노 페어   17.550%```'''

def simulate():
  cnt = {}
  doit = 10**4
  for i in range(doit):
    res = random_cards('test')
    cnt[res] = cnt.get(res,0) + 1
  res = sorted([k for k in cnt.items()],key=lambda x:x[1])
  for k,v in list(res):
    print (k,' ',"{:.3f}%".format(v/doit*100))

def simulate_match():
  doit = 10**5
  res = [match('test','1','2') for i in range(doit)]
  print ('A wins ', res.count('A'))
  print ('B wins ', res.count('B'))
  print ('Draws ', res.count('D'))

if __name__ == '__main__':
  simulate_match()
