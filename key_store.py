#네이버 이미지 검색용
X_NAVER_ID = ''
X_NAVER_SECRET  = ''
# 구글 이미지 검색용
X_GOOGLE_CX = ''
X_GOOGLE_KEY = ''
# DB, DISCORD
DISCORD_SECRET = ''
DB_GOOGLE_SHEET_ID = ''

def init_google_api():
  # 이건 google developer 를 신청해야한다..
  from oauth2client.service_account import ServiceAccountCredentials as SAC
  scope = ['https://spreadsheets.google.com/feeds']
  ginfo = {
   'auth': SAC.from_json_keyfile_name('config/google_info.json', scope), #여기에 파일을 둘것
  }
  return ginfo

def ensure_token(auth):
  if not auth.access_token or (hasattr(auth, 'access_token_expired') and auth.access_token_expired):
    import httplib2
    http = httplib2.Http()
    auth.refresh(http)
  return auth.access_token