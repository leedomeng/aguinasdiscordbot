import urllib
import aiohttp
import asyncio
import json

async def get_scores(dt):
  data = {"leId": "1"
  , "srId": "0,1,3,4,5,7,9"     # 1군 전체시리즈
  , "date": dt # ex) "20170601"
  }
  url = 'http://www.koreabaseball.com/ws/Main.asmx/GetKboGameList'
  headers = {
    'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8",
    'Accept' : "application/json, text/javascript",
  }
  enx = urllib.parse.urlencode(data)
  async with aiohttp.ClientSession() as session:
    async with session.post(url,headers=headers,data=enx) as resp:
      res_raw = await resp.read()
  res = json.loads(res_raw.decode('utf-8'))
  games = []
  for game in res['game']:
    # AWAY_NM (ie 넥센)
    # HOME_NM (ie LG)
    # T_SCORE_CN for away
    # B_SCORE_CN for home
    games += [(game['AWAY_NM'],game['HOME_NM'],game['T_SCORE_CN'],game['B_SCORE_CN'])]
  return games

if __name__ == '__main__':
  l = asyncio.get_event_loop()
  l.run_until_complete(get_scores())

