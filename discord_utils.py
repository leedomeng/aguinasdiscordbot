def first_vowels(text):
  def first_vowel(s):
    if s >= '가' and s <= '힣':
      return  ['ㄱ', 'ㄲ', 'ㄴ', 'ㄷ', 'ㄸ', 'ㄹ', 'ㅁ', 'ㅂ', 'ㅃ', 'ㅅ', 'ㅆ', 'ㅇ', 'ㅈ', 'ㅉ', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ'][(ord(s)-ord('가'))//588]
    return s
  return ''.join([first_vowel(s) for s in text])

if __name__ == '__main__':
  #test
  print (first_vowels('가나다라'))
  print (first_vowels('가1나2다3라'))
  print (first_vowels('동해물과 백두산이 abcd'))