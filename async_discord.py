# how to add bot to your server?
#  https://discordapp.com/oauth2/authorize?client_id=303350537653321733&scope=bot&permissions=0 
import discord
import asyncio
import aiohttp
import random
from games import poker
import discord_utils 
import googles 
import baseball_bot
import datetime
import key_store

client = discord.Client()
@client.event
async def on_ready():
  print('Logged in as')
  print(client.user.name)
  print(client.user.id)
  print('------')

async def async_get_request(url):
  async with aiohttp.ClientSession() as session:
    async with session.get(url) as resp:
      body = await resp.read()
  return body

from bs4 import BeautifulSoup as bs4
import pdb

async def get_exchange(cd):
  index = await async_get_request('http://finance.daum.net/exchange/exchangeDetail.daum?code=' + str(cd))
  try:
    soup = bs4(index, 'html.parser')
    i = soup.find('dl',{'id':'mainSise'})
    txt = i.text.split()
    return cd + ':' + ' '.join(txt[2:])
  except Exception as e:
    return ('히히 버그발싸!!! ' + str(e))

async def get_finance(cd,buyprice,fake=0):
  index = await async_get_request('http://finance.daum.net/item/main.daum?code=' + str(cd))
  index = index.decode('utf-8')
  #from daum
  try:
    i = index.find('document.title = "')
    j = index.find('\n',i)
    concern = index[i:j].split('"')[1::2]
    txt = ''.join(concern)
    a = concern[0]
    orgprice = a[a.rfind(' '):]
    price = int(orgprice.replace(',','')) + fake
    if fake>0:
      txt = txt.replace(orgprice,"{:,}".format(price))
    return (txt +' (구매가:{} 수익률:{:.2f}%)'.format(buyprice,(price-buyprice)*100.0/buyprice))
  except Exception as e:
    return ('히히 버그발싸!!! ' + str(e))

info = key_store.init_google_api()
jjalbang = {}
random_jjalbang = []
is_debug = False
finance = {}

async def load_finance():
  global finance
  finance = {}
  token = key_store.ensure_token(info['auth'])
  sheetId = key_store.DB_GOOGLE_SHEET_ID
  async with aiohttp.ClientSession() as session:
    headers = { 'Authorization' : 'Bearer ' + token }
    get_url = "https://sheets.googleapis.com/v4/spreadsheets/{0}/values/'주식'!A:C".format(sheetId)
    async with session.get(get_url,headers=headers) as resp:
      x = await resp.json()
      values = x['values']
      for data in values:
        try:
          user,code,price = data
          finance[user] = finance.get(user,[]) + [(code,int(price))]
        except:
          pass

async def load_jjal():
  global jjalbang
  jjalbang = {}
  token = key_store.ensure_token(info['auth'])
  sheetId = key_store.DB_GOOGLE_SHEET_ID
  async with aiohttp.ClientSession() as session:
    headers = { 'Authorization' : 'Bearer ' + token }
    get_url = "https://sheets.googleapis.com/v4/spreadsheets/{0}/values/A:B".format(sheetId)
    async with session.get(get_url,headers=headers) as resp:
      x = await resp.json()
      values = x['values']
      cnt = 0
      for vv in values:
        try:
          jjalbang[vv[0]] = vv[1]
          cnt += 1
        except:
          pass
    get_url2 = "https://sheets.googleapis.com/v4/spreadsheets/{0}/values/'랜덤짤방'!A:A".format(sheetId)
    async with session.get(get_url2,headers=headers) as resp:
      x = await resp.json()
      values = x['values']
      global random_jjalbang
      random_jjalbang = list([x[0] for x in values])
  return 'Updated DB - total {} rows, random {} row'.format(cnt, len(random_jjalbang))

import pdb
async def aguinas_cmd(message):
  global jjalbang, finance
  msg = message.content.strip()
  if msg.endswith('주식') and msg.startswith('!'):
    user = msg[1:-2]
    if user in finance:
      data = [asyncio.ensure_future(get_finance(code,price)) for code,price in finance[user]]
      done,pending = await asyncio.wait(data)
      return '\n'.join([f.result() for f in done])
  elif msg == '!주식 새로고침':
    res = await load_finance()
    return '주식 새로 불러오기 완료!'
  elif msg == '!짤':
    return ' '.join(jjalbang.keys()) + '\n' 
  elif msg == '!짤 DB':
    return 'https://goo.gl/K083NQ'
  elif msg == '!짤 새로고침':
    res = await load_jjal()
    return '짤방 새로 불러오기 완료! : ' + res
  elif msg == '!운세':
    if len(random_jjalbang) > 0:
      i = random.randint(0,len(random_jjalbang)-1)
      return random_jjalbang[i]
  elif msg in jjalbang.keys():
    return jjalbang[msg]
  return None

import codecs
async def common_cmd(message):
  msg = message.content.strip()
  if msg.startswith("!환율 "):
    try:
      code = msg.split(' ')[1]
      if code in ['달러','미국']:
        code = 'USD'
      if code in ['엔','일본']:
        code = 'JPY'
      if code in ['슈꽁']:
        code = 'AUD'
      x = await get_exchange(code)
      return x
    except Exception as e:
      return str(e)
  elif msg.startswith("!주사위 "):
    dimen = 6
    count = 1
    try:
      m = msg.split(' ')
      if 'd' in m[1].lower():
        mm = m[1].lower().split('d')
        count,dimen = int(mm[0]),int(mm[1])
      else:
        dimen = int(m[1])
    except Exception as e:
      return ('히히 버그발싸!!! ' + str(e))
    if count > 30:
      real = 10
      virtual = count - real 
      real_res = [random.randint(1,dimen) for _ in range(real)]
      virtual_res = int(random.gauss((dimen+1)/2*virtual, (virtual**0.5) * (((dimen**2-1)/12)**0.5)))
      return ' '.join(str(real_res)) + ' ... -> ' + str(sum(real_res) + virtual_res)
    res = [random.randint(1,dimen) for _ in range(count)]
    if count > 1:
      return ' '.join(str(res[:min(10,count)])) + '{0} -> '.format(' ...' if count > 10 else '') + str(sum(res))
    return str(res[0])
  elif msg.startswith("!선택 "):
    m = msg.split(' ')
    cnt = 1
    try:
      if len(m) < cnt+1:
        return 'ㅡㅡ?'
      return "/".join(random.sample(m[1:],cnt))
    except Exception as e:
      return ('히히 버그발싸!!! ' + str(e))
  elif msg == "!포커 확률":
    return poker.table()
  elif any([msg.startswith(head) for head in ["!포커","!포카","!ㅍㅋ"]]):
    m = msg.split()
    try:
      if len(m) <= 1:
        if len(msg) == 3:
          return poker.random_cards(message.server.name)
      else:
        opposite = m[1].lower()
        candies = []
        for u in message.server.members:
          if opposite in u.name.lower() or (u.nick and opposite in u.nick.lower()):
            candies.append(u)
          elif opposite in discord_utils.first_vowels(u.name) or (u.nick and opposite in discord_utils.first_vowels(u.nick)):
            candies.append(u)
        if len(candies) > 1:
          return '애매모호함! - ' + ' '.join([u.name for u in candies])
        if len(candies) < 1:
          return '해당하는 유저 없음!'
        if candies[0] == message.author:
          return poker.match(message.server.name, message.author.name, candies[0].name+"의 분신")
        return poker.match(message.server.name, message.author.name, candies[0].name)
    except Exception as e:
      return ('히히 버그발싸!!! ' + str(e))
  elif msg.startswith("!계산 "):
    vals = msg[len("!계산 "):].replace('^','**')
    if any([not c in '() 1234567890+-*/. ' for c in vals]):
      return '보안상의 이유로 () 1234567890+-*/. 외의 문자는 받지않음 ㅂㅂ'
    return eval(vals)
  elif msg.startswith("!이미지 "):
    #with codecs.open('image_search_log.log','a','utf-8') as fw:
    #  A = '{timestamp},{_id},{_id2},{name},{nick},{msg}\n'.format(timestamp=message.timestamp,_id=message.author.id, name=message.author.name, nick=message.author.nick, msg=msg,_id2=message.id)
    #  fw.write(A)
    return '서비스 종료!!'
  elif msg in ["!야구","!ㅇㄱ","!ㅃㄸ","!빠따"]:
    n = datetime.datetime.now()
    scores = await baseball_bot.get_scores("{0}{1:02d}{2:02d}".format(n.year,n.month,n.day))
    return "```" + "\n".join('{0} {2} - {3} {1}'.format(*x) for x in scores) + "```"

@client.event
async def on_message(message):
  try:
    if message.server.name == 'aguinas':
      x = await aguinas_cmd(message)
      if not x is None:
        if is_debug:
          print (x)
          return 
        await asyncio.sleep(0.5)
        await client.send_message(message.channel, x)
        return
    msg = message.content.strip()
    x = await common_cmd(message)
    if not x is None:
      if is_debug:
        print (x)
        return
      await asyncio.sleep(0.5)
      await client.send_message(message.channel, x)
      return
  except Exception as e:
    print (e)

async def test_finance():
  user = '이터'
  global finance
  if user in finance:
    data = [asyncio.ensure_future(get_finance(code,price)) for code,price in finance[user]]
    done,pending = await asyncio.wait(data)
    print ('\n'.join(sorted([f.result() for f in done])))

async def crawl_baseball():
  lastscores = [0,0,0,0,0]
  while True:
    scores = await baseball_bot.get_scores('20170501')
    send = []
    if lastscores != scores:
      for i in range(len(scores)):
        if lastscores[i] != scores[i]: 
          send.append('{0} {2} - {3} {1}'.format(*scores[i]))
      if len(send) > 0:
        msg = "\n".join(send)
        await client.send_message('bottest', msg)
      lastscores = scores
    await asyncio.sleep(60) #per 1 min

import os
if __name__ == '__main__':
  if 'debug' in os.sys.argv:
    is_debug = True
  l = asyncio.get_event_loop()
  l.run_until_complete(load_jjal())
  l.run_until_complete(load_finance())
  #f = asyncio.ensure_future(crawl_baseball())
  client.run(key_store.DISCORD_SECRET)

