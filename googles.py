import asyncio
import aiohttp 
import random
import urllib 
import key_store

async def async_get_request_json(ur):
  async with aiohttp.ClientSession() as session:
    async with session.get(url) as resp:
      js = await resp.json()
  return js

async def async_image_search(key):
  #todo: key url escape
  get_url = 'https://www.googleapis.com/customsearch/v1?q='+ urllib.parse.quote(key) + '&cx={cx}&searchType=image&key={key}'.format(cx=key_store.X_GOOGLE_CX, key=key_store.X_GOOGLE_KEY)
  result = await async_get_request_json(get_url)
  try:
    items = result['items']
    if len(items) < 1:
      return 'No result'
    return items[random.randint(0,len(items)-1)]['link']
  except:
    print (result)
    return 'Error'

async def async_image_search_naver(key):
  q = urllib.parse.quote(key)
  check_url = 'https://openapi.naver.com/v1/search/adult.json?query={q}'.format(q=q)
  get_url = 'https://openapi.naver.com/v1/search/image.json?query={q}&start={start}'.format(q=q,start=1)
  headers = { 'X-Naver-Client-Id' : key_store.X_NAVER_ID, 'X-Naver-Client-Secret' : key_store.X_NAVER_SECRET }
  async with aiohttp.ClientSession() as session:
    async with session.get(check_url, headers=headers) as resp:
      result = await resp.json()
      if result['adult'] == '1':
        return 'ㄴㄴㄴㄴㄴ 봇주인 은팔찌 보호장치 가동'
    async with session.get(get_url, headers=headers) as resp:
      result = await resp.json()
  try:
    items = result['items']
    if len(items) < 1:
      return 'No result'
    return items[random.randint(0,len(items)-1)]['link']
  except:
    print (result)
    return 'Error'

import os
if __name__ == '__main__':
  if 'debug' in os.sys.argv:
    is_debug = True
  l = asyncio.get_event_loop()
  e = asyncio.ensure_future(async_image_search_naver('1'))
  l.run_until_complete(e)
  print(e.result)
